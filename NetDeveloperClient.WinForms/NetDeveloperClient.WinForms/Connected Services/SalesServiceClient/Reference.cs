﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NetDeveloperClient.WinForms.SalesServiceClient {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="TrackSaleReportItem", Namespace="http://schemas.datacontract.org/2004/07/NetDeveloper.Model.DataContracts")]
    [System.SerializableAttribute()]
    public partial class TrackSaleReportItem : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int QuantityField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string TrackNameField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Quantity {
            get {
                return this.QuantityField;
            }
            set {
                if ((this.QuantityField.Equals(value) != true)) {
                    this.QuantityField = value;
                    this.RaisePropertyChanged("Quantity");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string TrackName {
            get {
                return this.TrackNameField;
            }
            set {
                if ((object.ReferenceEquals(this.TrackNameField, value) != true)) {
                    this.TrackNameField = value;
                    this.RaisePropertyChanged("TrackName");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="SalesServiceClient.ISalesService")]
    public interface ISalesService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISalesService/TrackSalesReport", ReplyAction="http://tempuri.org/ISalesService/TrackSalesReportResponse")]
        NetDeveloperClient.WinForms.SalesServiceClient.TrackSaleReportItem[] TrackSalesReport();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISalesService/TrackSalesReport", ReplyAction="http://tempuri.org/ISalesService/TrackSalesReportResponse")]
        System.Threading.Tasks.Task<NetDeveloperClient.WinForms.SalesServiceClient.TrackSaleReportItem[]> TrackSalesReportAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISalesService/Numero", ReplyAction="http://tempuri.org/ISalesService/NumeroResponse")]
        int Numero();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISalesService/Numero", ReplyAction="http://tempuri.org/ISalesService/NumeroResponse")]
        System.Threading.Tasks.Task<int> NumeroAsync();
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ISalesServiceChannel : NetDeveloperClient.WinForms.SalesServiceClient.ISalesService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class SalesServiceClient : System.ServiceModel.ClientBase<NetDeveloperClient.WinForms.SalesServiceClient.ISalesService>, NetDeveloperClient.WinForms.SalesServiceClient.ISalesService {
        
        public SalesServiceClient() {
        }
        
        public SalesServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public SalesServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SalesServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SalesServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public NetDeveloperClient.WinForms.SalesServiceClient.TrackSaleReportItem[] TrackSalesReport() {
            return base.Channel.TrackSalesReport();
        }
        
        public System.Threading.Tasks.Task<NetDeveloperClient.WinForms.SalesServiceClient.TrackSaleReportItem[]> TrackSalesReportAsync() {
            return base.Channel.TrackSalesReportAsync();
        }
        
        public int Numero() {
            return base.Channel.Numero();
        }
        
        public System.Threading.Tasks.Task<int> NumeroAsync() {
            return base.Channel.NumeroAsync();
        }
    }
}
