﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetDeveloperClient.WinForms
{
    public class ReportItem
    {
        public int Quantity { get; set; }
        public string TrackName { get; set; }
    }

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // crear el cliente del servicio
            var client = new SalesServiceClient.SalesServiceClient();
            var resultado = client.Numero();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            // crear el cliente del servicio
            var client = new SalesServiceClient.SalesServiceClient();
            var resultado = client.TrackSalesReport();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var client = new HttpClient();
            var response = client.GetAsync("http://localhost:52300/api/tracks/sales-report").Result;
            var rawData = response.Content.ReadAsStringAsync().Result;
            var data = JsonConvert.DeserializeObject<List<ReportItem>>(rawData);
        }
    }
}
