﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetDeveloper.Model
{
    [Serializable]
    public class Track
    {
        [Key]
        public int TrackId { get; set; }
        public string Name { get; set; }
        public int MediaTypeId { get; set; }
        public int? GenreId { get; set; }
        public int Milliseconds { get; set; }
        public decimal UnitPrice { get; set; }
        public int? Bytes { get; set; }
        public int? AlbumId { get; set; }

        public MediaType MediaType { get; set; }
        public Genre Genre { get; set; }
        public Album Album { get; set; }

        // relación con Invoice Line
        public ICollection<InvoiceLine> InvoiceLines { get; set; }
    }
}
