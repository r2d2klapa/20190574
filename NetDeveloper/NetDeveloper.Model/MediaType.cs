﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetDeveloper.Model
{
    [Serializable]
    public class MediaType
    {
        public int MediaTypeId { get; set; }
        public string Name { get; set; }
    }
}
