﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetDeveloper.Model
{
    public class InvoiceLine
    {
        public int InvoiceLineId { get; set; }
        public int TrackId { get; set; }
        public int Quantity { get; set; }

        public Track Track { get; set; }
    }
}
