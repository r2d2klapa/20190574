﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NetDeveloper.Model.ViewModels
{    
    public class TrackSaleReportItem
    {        
        public string TrackName { get; set; }        
        public int Quantity { get; set; }
    }
}
