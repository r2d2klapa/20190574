﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetDeveloper.Model.ViewModels
{
    /// <summary>
    /// Clase para obtener la cantidad de álbumes por artista
    /// </summary>
    public class ArtistTopAlbums
    {
        /// <summary>
        /// Id del artista
        /// </summary>
        public int ArtistId { get; set; }
        /// <summary>
        /// Nombre del artista
        /// </summary>
        public string ArtistName { get; set; }
        /// <summary>
        /// Cantidad de álbumes del artista
        /// </summary>
        public int AlbumCount { get; set; }
    }
}
