﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NetDeveloper.Model.DataContracts
{
    [DataContract]
    public class TrackSaleReportItem
    {
        [DataMember]
        public string TrackName { get; set; }
        [DataMember]
        public int Quantity { get; set; }
    }
}
