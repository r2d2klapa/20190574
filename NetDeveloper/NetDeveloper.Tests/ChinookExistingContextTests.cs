﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetDeveloper.DataAccess.EF;
using NetDeveloper.Model;
using System.Collections.Generic;
using System.Linq;
using NetDeveloper.DataAccess.Repositories;
using NetDeveloper.DataAccess;

namespace NetDeveloper.Tests
{
    [TestClass]
    public class ChinookExistingContextTests
    {
        [TestMethod]
        public void Seleccionar_Todos_Los_Artistas()
        {
            //using (var context = new ChinookExistingContext())
            //{
            //    var artistas = context.Artists.ToList();
            //    Assert.AreEqual(334, artistas.Count);
            //}

            var repo = new Repository<Artist>(new ChinookExistingContext());
            Assert.AreEqual(true, repo.GetAll().Count > 0);
        }

        [TestMethod]
        public void Seleccionar_Todos_Las_Canciones()
        {
            //using (var context = new ChinookExistingContext())
            //{
            //    var canciones = context.Tracks.ToList();
            //    Assert.AreEqual(3508, canciones.Count);
            //}

            var repo = new Repository<Track>(new ChinookExistingContext());
            Assert.AreEqual(true, repo.GetAll().Count > 0);
        }

        [TestMethod]
        public void ObtenerReporteVentasTrack()
        {
            using (var unit = new ChinookExistingUnitOfWork())
            {
                var resultado = unit.Tracks.GetTrackSalesReport();
                var resultadoLista = resultado.ToList();
                Assert.AreEqual(true, resultadoLista.Count > 0);
            }
        }
    }
}
