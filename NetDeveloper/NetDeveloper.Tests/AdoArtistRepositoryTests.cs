﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using NetDeveloper.DataAccess.AdoNet;
using NetDeveloper.Model;

namespace NetDeveloper.Tests
{
    [TestClass]
    public class AdoArtistRepositoryTests
    {
        [TestMethod]
        public void Count_Greater_Than_0()
        {
            // obtener la cadena de conexión del config
            var cs = ConfigurationManager.ConnectionStrings["ChinookCS"].ConnectionString;

            // crear una nueva instancia de ArtistRepository
            var artistRepository = new ArtistRepository(cs);

            // obtener el resultado de Count()
            var resultado = artistRepository.Count();

            // hacer la comprobación. Espero que al evaluar que resultado > 0, la condición sea verdadera
            Assert.AreEqual(true, resultado > 0);
        }

        [TestMethod]
        public void ObtenerPorId_Debe_Retornar_Un_Objeto_Vacio()
        {
            // obtener la cadena de conexión del config
            var cs = ConfigurationManager.ConnectionStrings["ChinookCS"].ConnectionString;

            // crear una nueva instancia de ArtistRepository
            var artistRepository = new ArtistRepository(cs);

            // obtener el artista
            var artist = artistRepository.ObtenerPorId(0);

            Assert.AreEqual(0, artist.ArtistId);
            Assert.IsNull(artist.Name);
        }

        [TestMethod]
        public void ObtenerPorId_Debe_Retornar_Un_Objeto_Con_Data()
        {
            // obtener la cadena de conexión del config
            var cs = ConfigurationManager.ConnectionStrings["ChinookCS"].ConnectionString;

            // crear una nueva instancia de ArtistRepository
            var artistRepository = new ArtistRepository(cs);

            // obtener el artista
            var artist = artistRepository.ObtenerPorId(1);

            Assert.AreEqual(1, artist.ArtistId);
            Assert.IsNotNull(artist.Name);
        }

        [TestMethod]
        public void InsertarArtista_Debe_Retornar_Positivo()
        {
            // obtener la cadena de conexión del config
            var cs = ConfigurationManager.ConnectionStrings["ChinookCS"].ConnectionString;

            // crear una nueva instancia de ArtistRepository
            var artistRepository = new ArtistRepository(cs);

            // insertar el artista
            var nuevoId = artistRepository.InsertarArtista(new Artist { Name = "Nuevo" });

            // que se cumpla que el nuevo id es negativo
            Assert.IsTrue(nuevoId > 0);
        }

        [TestMethod]
        public void ObtenerPorId_Debe_Retornar_Null()
        {
            // obtener la cadena de conexión del config
            var cs = ConfigurationManager.ConnectionStrings["ChinookCSInvalid"].ConnectionString;

            // crear una nueva instancia de ArtistRepository
            var artistRepository = new ArtistRepository(cs);

            // obtener el artista
            var artist = artistRepository.ObtenerPorId(2);

            Assert.IsNull(artist);
        }

        [TestMethod]
        public void InsertarArtista_Debe_Retornar_Negativo()
        {
            // obtener la cadena de conexión del config
            var cs = ConfigurationManager.ConnectionStrings["ChinookCSInvalid"].ConnectionString;

            // crear una nueva instancia de ArtistRepository
            var artistRepository = new ArtistRepository(cs);

            // insertar el artista
            var nuevoId = artistRepository.InsertarArtista(new Artist { Name = "Nuevo" });

            // que se cumpla que el nuevo id es negativo
            Assert.IsTrue(nuevoId < 0);
        }
    }
}
