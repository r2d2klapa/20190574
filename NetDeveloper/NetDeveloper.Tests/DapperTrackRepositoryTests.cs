﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetDeveloper.DataAccess.DapperRepo;
using NetDeveloper.Model;
using System.Configuration;
using System.Linq;

namespace NetDeveloper.Tests
{
    [TestClass]
    public class DapperTrackRepositoryTests
    {
        [TestMethod]
        public void Obtener_Todos_Debe_Devolver_Lista_No_Vacia()
        {
            var repo = new TrackRepository(ConfigurationManager.ConnectionStrings["ChinookCS"].ConnectionString);

            var resultado = repo.ObtenerTodo();

            var cantidadElementos = resultado.Count();

            Console.WriteLine(cantidadElementos);

            // hacer la comprobación
            Assert.IsTrue(cantidadElementos > 0);
        }

        [TestMethod]
        public void Obtener_Todos_Debe_Devolver_Lista_No_Vacia_Contrib()
        {
            var repo = new TrackRepository(ConfigurationManager.ConnectionStrings["ChinookCS"].ConnectionString);

            var resultado = repo.ObtenerTodoContrib();

            var cantidadElementos = resultado.Count();

            Console.WriteLine(cantidadElementos);

            // hacer la comprobación
            Assert.IsTrue(cantidadElementos > 0);
        }

        [TestMethod]
        public void Obtener_Track_Id_141()
        {
            var repo = new TrackRepository(ConfigurationManager.ConnectionStrings["ChinookCS"].ConnectionString);

            // invocar al método para obtener por ID
            var resultado = repo.ObtenerPorIdSP(141);

            Console.WriteLine($"El nombre es {resultado.Name}");

            // hacer la comprobación
            Assert.IsTrue(resultado.TrackId > 0);
        }

        [TestMethod]
        public void Obtener_Track_Id_142()
        {
            var repo = new TrackRepository(ConfigurationManager.ConnectionStrings["ChinookCS"].ConnectionString);

            // invocar al método para obtener por ID
            var resultado = repo.ObtenerPorId(142);

            Console.WriteLine($"El nombre es {resultado.Name}");

            // hacer la comprobación
            Assert.IsTrue(resultado.TrackId > 0);
        }

        [TestMethod]
        public void Obtener_Track_Id_2()
        {
            var repo = new TrackRepository(ConfigurationManager.ConnectionStrings["ChinookCS"].ConnectionString);

            // invocar al método para obtener por ID
            var resultado = repo.ObtenerPorId(2);

            Console.WriteLine($"El nombre es {resultado.Name}");

            // manejar el caso en que los bytes sean nulos
            int bytes = resultado.Bytes ?? 1111;
            Console.WriteLine($"La cantidad de bytes es {bytes}");

            // hacer la comprobación
            Assert.IsTrue(resultado.TrackId > 0);
        }

        [TestMethod]
        public void Insertar_Track_Nuevo()
        {
            var repo = new TrackRepository(ConfigurationManager.ConnectionStrings["ChinookCS"].ConnectionString);

            // declarar el objeto a insertar
            var nuevoTrack = new Track
            {
                Name = "Nueva canción",
                MediaTypeId = 1,
                Milliseconds = 60000,
                UnitPrice = 2.3M,
            };

            var resultado = repo.Insertar(nuevoTrack);

            // hacer la comprobación
            Assert.AreEqual(1, resultado);
        }

        [TestMethod]
        public void Insertar_Track_Nuevo_Contrib()
        {
            var repo = new TrackRepository(ConfigurationManager.ConnectionStrings["ChinookCS"].ConnectionString);

            // declarar el objeto a insertar
            var nuevoTrack = new Track
            {
                Name = "Nueva canción Contrib",
                MediaTypeId = 1,
                Milliseconds = 60000,
                UnitPrice = 2.3M,
            };

            var resultado = repo.InsertarContrib(nuevoTrack);

            Console.WriteLine($"El nuevo id es: {resultado}");

            // hacer la comprobación
            Assert.IsTrue(resultado > 0);
        }
    }
}
