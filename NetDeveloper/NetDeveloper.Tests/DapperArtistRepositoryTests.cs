﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using NetDeveloper.DataAccess.DapperRepo;
using NetDeveloper.Model;
using System.Collections.Generic;

namespace NetDeveloper.Tests
{
    [TestClass]
    public class DapperArtistRepositoryTests
    {
        [TestMethod]
        public void InsertarArtistaBatch_Debe_Retornar_Verdadero()
        {
            // obtener la cadena de conexión del config
            var cs = ConfigurationManager.ConnectionStrings["ChinookCS"].ConnectionString;

            // crear una nueva instancia de ArtistRepository
            var artistRepository = new ArtistRepository(cs);

            var listaArtistas = new List<Artist>
            {
                new Artist {Name="Batch 11"},
                new Artist {Name="Batch 22"},
                new Artist {Name="Batch 33"},
                new Artist {Name="Batch 44"},
            };

            // insertar el artista
            var resultado = artistRepository.InsertarArtistaBatch(listaArtistas);

            // que se cumpla que el nuevo id es negativo
            Assert.IsTrue(resultado);
        }

        [TestMethod]
        public void Insertar_Multiple_SP()
        {
            var repo = new ArtistRepository(ConfigurationManager.ConnectionStrings["ChinookCS"].ConnectionString);

            var resultado = repo.InsertarMultipleSP();

            // hacer la comprobación
            Assert.AreEqual(2, resultado);
        }
    }
}
