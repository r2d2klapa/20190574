﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetDeveloper.DataAccess.EF;
using NetDeveloper.Model;
using System.Collections.Generic;

namespace NetDeveloper.Tests
{
    [TestClass]
    public class ChinookContextTests
    {
        [TestMethod]
        public void Insertar_Un_Nuevo_Artista()
        {
            using (var context = new ChinookContext())
            {
                var nuevoArtista = new Artist
                {
                    Name = "Armonía 10"
                };

                context.Artists.Add(nuevoArtista);

                // guardar cambios
                context.SaveChanges();

                Assert.IsTrue(nuevoArtista.ArtistId > 0);
            }
        }

        [TestMethod]
        public void Insertar_Un_Nuevo_Artista_Con_Albumes()
        {
            using (var context = new ChinookContext())
            {
                var nuevoArtista = new Artist
                {
                    Name = "Metallica",
                    Albums = new List<Album>
                    {
                        new Album { Title = "Black Album" },
                        new Album { Title = "Kill'em all" }
                    }
                };

                context.Artists.Add(nuevoArtista);

                // guardar cambios
                context.SaveChanges();

                Assert.IsTrue(nuevoArtista.ArtistId > 0);
            }
        }
    }
}
