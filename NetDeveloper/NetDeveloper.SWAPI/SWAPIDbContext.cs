﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace NetDeveloper.SWAPI
{
    public class SWAPIDbContext : DbContext
    {
        public SWAPIDbContext() : base("SWAPIConnection")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // especificar el schema
            //modelBuilder.HasDefaultSchema("Admin");
            // especificar el nombre de la tabla que se cerará para la entidad Person
            //modelBuilder.Entity<Person>().ToTable("tbl_person2");

            // para desactivar la pluralización
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            // para especificar los tipos de dato de las columnas
            modelBuilder.Entity<Person>()
                .Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(200)
                .HasColumnType("varchar");
        }

        public DbSet<Person> People { get; set; }
    }
}
