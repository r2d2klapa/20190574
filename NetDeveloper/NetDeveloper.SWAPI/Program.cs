﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace NetDeveloper.SWAPI
{
    class Program
    {
        static void Main(string[] args)
        {
            // agregar un registro a la tabla Person
            //using (var context = new SWAPIDbContext())
            //{
            //    // agregar 1 registro
            //    var nuevoRegistro = new Person
            //    {
            //        Name = "Nueva persona"
            //    };

            //    // agregar el registro al db set correspondiente
            //    context.People.Add(nuevoRegistro);

            //    // guardar cambios
            //    context.SaveChanges();
            //}

            //return;

            Console.WriteLine("Cliente para Consultar SWAPI\n\n");

            /*
            // 1. Crear el cliente http
            var client = new HttpClient();

            // 2. Definir la url base del cliente
            client.BaseAddress = new Uri(@"https://swapi.co/api/");

            // 3. Obtener la información del personaje 1
            var respuesta = client.GetAsync("people/1").Result;

            Console.WriteLine(respuesta.StatusCode);

            var responseContent = respuesta.Content.ReadAsStringAsync().Result;

            // 4. Convertir de string a objetos c# (deserialización - parse)
            var parsedContent = JsonConvert.DeserializeObject<People>(responseContent);

            //Console.WriteLine(responseContent);

            Console.WriteLine($"El nombre del personaje es: {parsedContent.Name}");
            Console.WriteLine($"La talla del personaje es: {parsedContent.Height}");
            Console.WriteLine($"El color de cabello del personaje es: {parsedContent.HairColor}");

            */

            // obtener toda la lista de people
            GetAllPeopleList("people");

            var lista = peopleList;

            // incializar el swapi context y crear la BD
            using (var context = new SWAPIDbContext())
            {
                //context.Database.CreateIfNotExists();

                // insertar la data (en rango)
                context.People.AddRange(lista);

                // guardar cambios
                context.SaveChanges();
            }

            // lista desordenada
            Console.WriteLine("Lista de people:\n");
            Pintarlista(lista);

            // ejemplos con LINQ
            // 1. Ordenamiento (query syntax)
            var listaOrdenadaQS = from l in lista
                                  orderby l.Name
                                  select l;

            // 1. Ordenamiento (fluent syntax)
            var listaOrdenadaFS = lista.OrderByDescending(l => l.Name);

            Console.WriteLine("\nLista de people ordenada:\n");
            Pintarlista(listaOrdenadaQS.ToList());

            Console.WriteLine("\nLista de people ordenada (descendente):\n");
            Pintarlista(listaOrdenadaFS.ToList());

            // 2. Filtrar la lista (query syntax)
            var encontrarC3PO = from l in lista
                                where l.Name.ToUpper().Contains("3PO")
                                select l;

            Console.WriteLine("\nEncontrar C3PO:\n");
            Pintarlista(encontrarC3PO.ToList());

            // 3. Encontrar y devolver solo el primer elemento bajo una condición
            // Encontrar la primera ocurrencia de todos los nombres que empiecen con la letra B
            var primeraOcurrencia = lista.FirstOrDefault(l => l.Name.ToUpper().StartsWith("B"));

            Console.WriteLine($"Primera ocurrencia nombre con B: {primeraOcurrencia.Name}");

            // 4. Agrupamiento por color de cabello
            var grupos = lista.GroupBy(l => l.HairColor);

            Console.WriteLine("\nGrupos por color de cabello:\n");
            foreach (var grupo in grupos)
            {
                Console.WriteLine($"\n{grupo.Key}");
                Console.WriteLine("---------");
                foreach (var item in grupo)
                {
                    Console.WriteLine($"Nombre: {item.Name} - Color de cabello: {item.HairColor}");
                }
            }

            Console.ReadLine();
        }

        static void Pintarlista(List<Person> lista)
        {
            // va a pintar en consola todos los elemento de la lista enviada como parámetro
            foreach (var item in lista)
            {
                Console.WriteLine($"Nombre: {item.Name}");
            }
        }

        static List<Person> peopleList;

        static void GetAllPeopleList(string next)
        {
            // inicializar la lista si es nula
            if (peopleList == null)
            {
                peopleList = new List<Person>();
            }

            // validar que next no sea nulo. Si es nulo, detener la recursividad
            if (next == null)
            {
                return;
            }

            // 1. Crear el cliente http
            var client = new HttpClient();

            // 2. Definir la url base del cliente
            client.BaseAddress = new Uri(@"https://swapi.co/api/");

            // 3. Obtener la información de todos los personajes
            var respuesta = client.GetAsync(next).Result;

            // 4. Convertir el contenido de la respuesta a string
            var stringResponse = respuesta.Content.ReadAsStringAsync().Result;

            // 5. Parsear la respuesta
            var parsedResponse = JsonConvert.DeserializeObject<PeopleListResult>(stringResponse);

            // 6. Agregar los items del campo Results
            peopleList.AddRange(parsedResponse.Results);

            // 7. Volver a llamar a la función con el nuevo valor de next
            GetAllPeopleList(parsedResponse.Next);
        }
    }

    //[Table("tbl_persona")]
    public class Person
    {
        public int Id { get; set; }
        //[Required]
        //[MaxLength(100)]
        public string Name { get; set; }
        public string Height { get; set; }
        [JsonProperty("hair_color")]
        public string HairColor { get; set; }
    }

    public class PeopleListResult
    {
        public int Count { get; set; }
        public string Next { get; set; }
        public IEnumerable<Person> Results { get; set; }
    }
}
