﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetDeveloper.DataAccess.Repositories;
using NetDeveloper.Model;

namespace NetDeveloper.DataAccess
{
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Método para guardar los cambios en la BD
        /// </summary>
        /// <returns></returns>
        int Commit();

        #region Repositorios
        IArtistRepository Artists { get; }
        IRepository<Genre> Genres { get; }
        ITrackRepository Tracks { get; }
        IUserRepository Users { get; }
        IRepository<Customer> Customers { get; }
        #endregion
    }
}
