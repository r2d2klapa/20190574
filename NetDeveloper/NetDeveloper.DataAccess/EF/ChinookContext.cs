﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using NetDeveloper.Model;

namespace NetDeveloper.DataAccess.EF
{
    public class ChinookContext : DbContext
    {
        public ChinookContext() : base("ChinookConnectionEF")
        //public ChinookContext() : base("ChinookConnectionEFAzure")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // quitar la convención de pluralización de tablas
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public DbSet<Artist> Artists { get; set; }
        public DbSet<Album> Albums { get; set; }
        public DbSet<Track> Tracks { get; set; }
        public DbSet<MediaType> MediaTypes { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
