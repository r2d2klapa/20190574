﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using NetDeveloper.Model;

namespace NetDeveloper.DataAccess.EF
{
    /// <summary>
    /// Este contexto se conecta con una BD existente y obviamos la lógica para tratar de crear la BD si no existe
    /// </summary>
    public class ChinookExistingContext : DbContext
    {
        public ChinookExistingContext() : base("ChinookCS")
        {
            // va a deshabilitar la funcionalidad del contexto para que revise si tiene que crear una BD
            Database.SetInitializer<ChinookExistingContext>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // quitar la convención de pluralización de tablas
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public DbSet<Artist> Artists { get; set; }
        public DbSet<Album> Albums { get; set; }
        public DbSet<Track> Tracks { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<InvoiceLine> InvoiceLines { get; set; }
        public DbSet<Customer> Customers { get; set; }
    }
}
