﻿namespace NetDeveloper.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CambiosPendientes : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Track", "AlbumId", "dbo.Album");
            DropForeignKey("dbo.Track", "GenreId", "dbo.Genre");
            DropIndex("dbo.Track", new[] { "GenreId" });
            DropIndex("dbo.Track", new[] { "AlbumId" });
            CreateTable(
                "dbo.InvoiceLine",
                c => new
                    {
                        InvoiceLineId = c.Int(nullable: false, identity: true),
                        TrackId = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.InvoiceLineId)
                .ForeignKey("dbo.Track", t => t.TrackId, cascadeDelete: true)
                .Index(t => t.TrackId);
            
            AlterColumn("dbo.Track", "GenreId", c => c.Int());
            AlterColumn("dbo.Track", "AlbumId", c => c.Int());
            CreateIndex("dbo.Track", "GenreId");
            CreateIndex("dbo.Track", "AlbumId");
            AddForeignKey("dbo.Track", "AlbumId", "dbo.Album", "AlbumId");
            AddForeignKey("dbo.Track", "GenreId", "dbo.Genre", "GenreId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Track", "GenreId", "dbo.Genre");
            DropForeignKey("dbo.Track", "AlbumId", "dbo.Album");
            DropForeignKey("dbo.InvoiceLine", "TrackId", "dbo.Track");
            DropIndex("dbo.InvoiceLine", new[] { "TrackId" });
            DropIndex("dbo.Track", new[] { "AlbumId" });
            DropIndex("dbo.Track", new[] { "GenreId" });
            AlterColumn("dbo.Track", "AlbumId", c => c.Int(nullable: false));
            AlterColumn("dbo.Track", "GenreId", c => c.Int(nullable: false));
            DropTable("dbo.InvoiceLine");
            CreateIndex("dbo.Track", "AlbumId");
            CreateIndex("dbo.Track", "GenreId");
            AddForeignKey("dbo.Track", "GenreId", "dbo.Genre", "GenreId", cascadeDelete: true);
            AddForeignKey("dbo.Track", "AlbumId", "dbo.Album", "AlbumId", cascadeDelete: true);
        }
    }
}
