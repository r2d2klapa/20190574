﻿namespace NetDeveloper.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DniMaxLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.User", "Dni", c => c.String(nullable: false, maxLength: 8));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.User", "Dni", c => c.String(nullable: false));
        }
    }
}
