﻿// <auto-generated />
namespace NetDeveloper.DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.3.0")]
    public sealed partial class CreateTableUser : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CreateTableUser));
        
        string IMigrationMetadata.Id
        {
            get { return "202002011645278_CreateTableUser"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
