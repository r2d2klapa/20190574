﻿namespace NetDeveloper.DataAccess.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using NetDeveloper.Model;

    internal sealed class Configuration : DbMigrationsConfiguration<NetDeveloper.DataAccess.EF.ChinookContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "NetDeveloper.DataAccess.EF.ChinookContext";
        }

        protected override void Seed(NetDeveloper.DataAccess.EF.ChinookContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.            
        }
    }
}
