﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetDeveloper.Model;
using System.Data.SqlClient;
using Dapper;
using Dapper.Contrib.Extensions;

namespace NetDeveloper.DataAccess.DapperRepo
{
    public class TrackRepository
    {
        private readonly string _connectionString;

        public TrackRepository(string connectionString)
        {
            _connectionString = connectionString;

            // para deshabilitar la pluralización en dapper contrib
            SqlMapperExtensions.TableNameMapper = type => { return type.Name; };
        }

        public IEnumerable<Track> ObtenerTodo()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var resultado = connection.Query<Track>("select * from track");
                return resultado;
            }
        }

        public IEnumerable<Track> ObtenerTodoContrib()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var resultado = connection.GetAll<Track>();
                return resultado;
            }
        }

        public IEnumerable<Track> ObtenerTodoSP()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var resultado = connection.Query<Track>("ObtenerTrackSP", commandType: System.Data.CommandType.StoredProcedure);
                return resultado;
            }
        }

        public Track ObtenerPorIdSP(int trackId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.QueryFirst<Track>("TrackById", new { TrackId = trackId }, commandType: System.Data.CommandType.StoredProcedure);
            }
        }

        public Track ObtenerPorId(int trackId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                // forma insegura (pero funciona)
                //var q2 = $"select * from Track where TrackId = {trackId}";
                //return connection.QueryFirst<Track>(q2);                

                // definir el query a ejecutar con parámetros
                var query = @"select * from 
                                Track 
                              where 
                                TrackId = @TrackId";

                // ejecutar el query con dapper
                return connection.QueryFirst<Track>(query, new { TrackId = trackId });
            }
        }

        public int Insertar(Track newTrack)
        {
            var query = @"insert into track
                          (Name, MediaTypeId, Milliseconds, UnitPrice)
                          values
                          (@Name, @MediaTypeId, @Milliseconds, @UnitPrice)";

            using (var connection = new SqlConnection(_connectionString))
            {
                var resultadoInsert = connection.Execute(query, newTrack);
                return resultadoInsert;
            }
        }

        public int InsertarContrib(Track newTrack)
        {
            using(var connection = new SqlConnection(_connectionString))
            {
                return (int)connection.Insert<Track>(newTrack);
            }
        }
    }
}
