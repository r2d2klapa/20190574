﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetDeveloper.Model;
using Dapper;
using System.Data.SqlClient;
using System.Transactions;

namespace NetDeveloper.DataAccess.DapperRepo
{
    public class ArtistRepository
    {
        private readonly string _connectionString;

        public ArtistRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public int Count()
        {
            var query = "select count(1) from Artist";

            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.ExecuteScalar<int>(query);
            }
        }

        public IEnumerable<Artist> ObtenerLista()
        {
            var query = "select ArtistId, Name from Artist order by 2";

            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Artist>(query);
            }
        }

        public Artist ObtenerPorId(int id)
        {
            var query = $"select ArtistId, Name from Artist where ArtistId = {id}";

            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.QueryFirstOrDefault<Artist>(query);
            }
        }

        public int InsertarArtista(Artist nuevoArtista)
        {
            var storedProcedure = "InsertArtist";

            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.ExecuteScalar<int>(storedProcedure, new { Name = nuevoArtista.Name }, commandType: System.Data.CommandType.StoredProcedure);
            }
        }

        public bool InsertarArtistaBatch(List<Artist> listaArtistas)
        {
            var storedProcedure = "InsertArtist";
            using (var transaction = new TransactionScope())
            {
                using (var connection = new SqlConnection(_connectionString))
                {

                    try
                    {
                        foreach (var artista in listaArtistas)
                        {
                            System.Threading.Thread.Sleep(2000);
                            var nuevoId = connection.ExecuteScalar<int>(storedProcedure, new { Name = artista.Name }, commandType: System.Data.CommandType.StoredProcedure);
                        }

                        // si ninguna inserción falló entonces confirmamos los cambios
                        transaction.Complete();
                        return true;
                    }
                    catch (Exception)
                    {
                        // deshacer los cambios que se hayan hecho en BD
                        transaction.Dispose();
                        return false;
                    }
                }
            }
        }


        public int InsertarMultipleSP()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var resultadoInsert = connection.ExecuteScalar<int>("InsertMultipleArtist", commandType: System.Data.CommandType.StoredProcedure);
                return resultadoInsert;
            }
        }
    }
}
