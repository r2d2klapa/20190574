﻿using NetDeveloper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetDeveloper.DataAccess.Repositories
{
    public interface ITrackRepository : IRepository<Track>
    {
        IEnumerable<NetDeveloper.Model.DataContracts.TrackSaleReportItem> GetTrackSalesReportContract();
        IEnumerable<NetDeveloper.Model.ViewModels.TrackSaleReportItem> GetTrackSalesReport();
    }
}
