﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetDeveloper.Model;

namespace NetDeveloper.DataAccess.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        User GetUserByEmailAndPassword(string email, string password);
    }
}
