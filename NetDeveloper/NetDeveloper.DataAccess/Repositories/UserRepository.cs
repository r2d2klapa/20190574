﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetDeveloper.Model;

namespace NetDeveloper.DataAccess.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(DbContext context) : base(context)
        {
        }

        public User GetUserByEmailAndPassword(string email, string password)
        {
            return _context.Set<User>().FirstOrDefault(u => u.Email == email && u.Password == password);
        }
    }
}
