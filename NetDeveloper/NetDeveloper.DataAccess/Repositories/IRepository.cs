﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetDeveloper.DataAccess.Repositories
{
    public interface IRepository<T>
    {
        /// <summary>
        /// Método para obtener un elemento del tipo T de la base de datos
        /// </summary>
        /// <param name="id">El ID o la llave primaria de la tabla</param>
        /// <returns>El objeto del tipo T en base a su ID</returns>
        T GetById(int id);

        /// <summary>
        /// Método para retornar una lista de elementos del tipo de T de la BD
        /// </summary>
        /// <returns>La lista dle tipo T</returns>
        List<T> GetAll();

        /// <summary>
        /// Método para insertar un nuevo registro del tipo T en la BD
        /// </summary>
        /// <param name="newEntity">El nuevo registro</param>
        void Insert(T newEntity);

        void Delete(T entity);
    }
}
