﻿using NetDeveloper.Model;
using ReportItemContract = NetDeveloper.Model.DataContracts.TrackSaleReportItem;
using ReportItem = NetDeveloper.Model.ViewModels.TrackSaleReportItem;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetDeveloper.Model.ViewModels;

namespace NetDeveloper.DataAccess.Repositories
{
    public class TrackRepository : Repository<Track>, ITrackRepository
    {
        public TrackRepository(DbContext context) : base(context)
        {
        }

        public IEnumerable<ReportItem> GetTrackSalesReport()
        {
            return
                from t in _context.Set<Track>()
                join gr in (
                from il in _context.Set<InvoiceLine>()
                group il by il.TrackId into g
                select new { GTrackId = g.Key, GVentas = g.Sum(i => i.Quantity) })
                on t.TrackId equals gr.GTrackId into result
                from resultitem in result.DefaultIfEmpty(new { GTrackId = 0, GVentas = 0 })
                select new ReportItem { TrackName = t.Name, Quantity = resultitem.GVentas };
        }

        public IEnumerable<ReportItemContract> GetTrackSalesReportContract()
        {
            // obtener una lista de elementos TrackSaleReportItem
            // que muestre cuantas veces se vendió cada canción
            // TrackName -> Quantity
            // Cancion1 -> 2
            // Cancion2 -> 1
            // Cancion3 -> 1

            //return _context.Set<InvoiceLine>()
            //    .GroupBy(il => il.Track)
            //    .Select(g => new TrackSaleReportItem
            //    {
            //        TrackName = g.Key.Name,
            //        Quantity = g.Sum(i => i.Quantity)
            //    });

            return
                from t in _context.Set<Track>()
                join gr in (
                from il in _context.Set<InvoiceLine>()
                group il by il.TrackId into g
                select new { GTrackId = g.Key, GVentas = g.Sum(i => i.Quantity) })
                on t.TrackId equals gr.GTrackId into result
                from resultitem in result.DefaultIfEmpty(new { GTrackId = 0, GVentas = 0 })
                select new ReportItemContract { TrackName = t.Name, Quantity = resultitem.GVentas };
        }
    }
}
