﻿using NetDeveloper.Model;
using NetDeveloper.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetDeveloper.DataAccess.Repositories
{
    public class ArtistRepository : Repository<Artist>, IArtistRepository
    {
        public ArtistRepository(DbContext context) : base(context)
        {
        }

        public List<ArtistTopAlbums> GetTopNArtists(int topN)
        {
            // inicializar la lista que vamos a devolver
            var lista = new List<ArtistTopAlbums>();

            // obtener la data de la BD
            var resultadoConsulta = _context.Set<Artist>().Include(a => a.Albums)
                .Select(queryResult =>
                    new ArtistTopAlbums
                    {
                        ArtistId = queryResult.ArtistId,
                        ArtistName = queryResult.Name,
                        AlbumCount = queryResult.Albums.Count
                    })
                .OrderByDescending(selectResult => selectResult.AlbumCount)
                .Take(topN);

            // asignar los resultados a la lista (ejecuta el query en la BD)
            lista = resultadoConsulta.ToList();

            return lista;
        }
    }
}
