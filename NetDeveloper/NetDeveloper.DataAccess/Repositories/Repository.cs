﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace NetDeveloper.DataAccess.Repositories
{    
    public class Repository<T> : IRepository<T> where T : class
    {
        // declarar un contexto genérico para trabajar con los métodos
        protected readonly DbContext _context;

        public Repository(DbContext context)
        {
            _context = context;
        }

        public void Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
        }

        public List<T> GetAll()
        {
            return _context.Set<T>().ToList();
        }

        public T GetById(int id)
        {
            return _context.Set<T>().Find(id);
        }

        public void Insert(T newEntity)
        {
            _context.Set<T>().Add(newEntity);
        }
    }
}
