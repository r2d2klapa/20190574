﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetDeveloper.Model.ViewModels;
using NetDeveloper.Model;

namespace NetDeveloper.DataAccess.Repositories
{
    public interface IArtistRepository : IRepository<Artist>
    {
        List<ArtistTopAlbums> GetTopNArtists(int topN);
    }
}
