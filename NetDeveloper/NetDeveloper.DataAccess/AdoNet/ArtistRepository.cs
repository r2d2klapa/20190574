﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using NetDeveloper.Model;

namespace NetDeveloper.DataAccess.AdoNet
{
    public class ArtistRepository
    {
        // la cadena de conexión
        private readonly string _connectionString;

        public ArtistRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// Método que sirve para contar la cantidad de registro de la tabla
        /// </summary>
        /// <returns>Cantidad de registros de la tabla</returns>
        public int Count()
        {
            // definir el query
            var query = "SELECT COUNT(1) FROM Artist";

            // crear la conexión y hacer la consulta
            using (var connection = new SqlConnection(_connectionString))
            {
                // crear el comando
                var command = new SqlCommand(query, connection);

                // abrir la coneción con la BD
                connection.Open();

                // ejecutar el comando
                var resultado = (int)command.ExecuteScalar();

                // devolver la cantidad
                return resultado;
            }
        }

        public Artist ObtenerPorId(int id)
        {
            var query = $"SELECT ArtistId, Name FROM Artist WHERE ArtistId = {id}";

            using (var connection = new SqlConnection(_connectionString))
            {
                var command = new SqlCommand(query, connection);

                try
                {
                    connection.Open();

                    var reader = command.ExecuteReader();

                    var artist = new Artist();
                    while (reader.Read())
                    {
                        artist.ArtistId = Convert.ToInt32(reader["ArtistId"]);
                        artist.Name = reader["Name"].ToString();
                    }

                    return artist;
                }
                catch (Exception)
                {
                    // log del error
                    return null;
                }

            }
        }

        public int InsertarArtista(Artist nuevoArtista)
        {
            // declarar el nombre del stored procedure
            var storedName = "InsertArtist";

            // ejecutar el SP
            using (var connection = new SqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();
                    var command = new SqlCommand(storedName, connection);

                    // establecer que el comando es para ejecutar un sp
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // agregar los parámetros
                    command.Parameters.AddWithValue("@name", nuevoArtista.Name);

                    // ejecutar el SP y retornar el nuevo id insertado
                    return (int)command.ExecuteScalar();
                }
                catch (Exception)
                {
                    // retornar un valor negativo apra saber que el método ha fallado
                    return -1;
                }
            }
        }

        public IEnumerable<Artist> ObtenerLista()
        {
            var query = "select ArtistId as ID, Name from Artist order by 2";

            using (var connection = new SqlConnection(_connectionString))
            {
                var command = new SqlCommand(query, connection);

                connection.Open();

                var reader = command.ExecuteReader();

                // declarar la lista que vamos a devolver (lista vacía)
                var lista = new List<Artist>();

                while (reader.Read())
                {
                    var artista = new Artist
                    {
                        ArtistId = Convert.ToInt32(reader["ID"]),
                        Name = reader["Name"].ToString()
                    };

                    // agregar el objeto a la lista
                    lista.Add(artista);
                }

                return lista;
            }
        }
    }
}
