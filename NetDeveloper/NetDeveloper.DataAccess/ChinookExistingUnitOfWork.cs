﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetDeveloper.DataAccess.Repositories;
using NetDeveloper.Model;
using NetDeveloper.DataAccess.EF;

namespace NetDeveloper.DataAccess
{
    public class ChinookExistingUnitOfWork : IUnitOfWork
    {
        // crear el contexto sobre el cual se va a trabajar
        private readonly ChinookExistingContext _context;

        public ChinookExistingUnitOfWork()
        {
            // aca se inicializan todas las interfaces
            _context = new ChinookExistingContext();

            // inicializar los repositorios
            Artists = new ArtistRepository(_context);
            Genres = new Repository<Genre>(_context);
            Tracks = new TrackRepository(_context);
            Users = new UserRepository(_context);
            Customers = new Repository<Customer>(_context);
        }

        public IArtistRepository Artists { get; private set; }

        public IRepository<Genre> Genres { get; private set; }

        public ITrackRepository Tracks { get; private set; }

        public IUserRepository Users { get; private set; }

        public IRepository<Customer> Customers { get; private set; }

        public int Commit()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            // cuando temrine la ejecución de la clase dentro de un using llamar al dispose del context
            _context.Dispose();
        }
    }
}
