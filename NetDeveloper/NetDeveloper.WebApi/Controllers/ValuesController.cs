﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NetDeveloper.WebApi.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return $"value: {id}";
        }

        [HttpPost]
        [Route("api/ruta/{id}/{nombre}")]
        [Route("api/ruta2/{id}/{nombre}")]
        [Route("api/values/{id}/{nombre}")]
        public string GetParams(int id, string nombre)
        {
            return $"value: {id} - nombre: {nombre}";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
