﻿using NetDeveloper.DataAccess;
using NetDeveloper.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace NetDeveloper.WebApi.Controllers
{
    public class TracksController : ApiController
    {
        public string Get()
        {
            return "algun texto";
        }

        [HttpGet]
        [Route("api/tracks/sales-report")]
        public IEnumerable<TrackSaleReportItem> SalesReport()
        {            
            using (var unit = new ChinookExistingUnitOfWork())
            {
                var query = unit.Tracks.GetTrackSalesReport();
                return query.ToList();
            }
        }
    }
}