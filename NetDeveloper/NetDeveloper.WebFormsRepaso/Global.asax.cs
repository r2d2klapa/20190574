﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace NetDeveloper.WebFormsRepaso
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // cofigurar el logger en base a lo indicado en el web config
            log4net.Config.XmlConfigurator.Configure();
        }

        protected void Application_Error(Object sender, EventArgs e)
        {
            // obtener la excepción
            var ex = Server.GetLastError();
            var logger = LogManager.GetLogger(typeof(Global));
            // escribir el mensaje de error
            logger.Error(ex);
        }
    }
}