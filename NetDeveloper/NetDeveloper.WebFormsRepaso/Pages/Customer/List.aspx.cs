﻿using NetDeveloper.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetDeveloper.WebFormsRepaso.Pages.Customer
{
    public partial class List : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CargarData();
            }
        }

        protected void CargarData(int pageIndex = 0)
        {
            gvCustomer.PageIndex = pageIndex;
            using (var uow = new ChinookExistingUnitOfWork())
            {
                gvCustomer.DataSource = uow.Customers.GetAll();
            }

            gvCustomer.DataBind();
        }

        protected void gvCustomer_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CargarData(e.NewPageIndex);
        }

        protected void btnInsertar_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "openModalKey", "openModal()", true);
            litTituloModal.Text = "Insertar un registro";
            hdnCustomerId.Value = "0";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            // obtener el customerId
            if (int.TryParse(hdnCustomerId.Value, out int customerId))
            {
                if (customerId == 0)
                {
                    // si customer id es 0, entonces insertar
                    InsertarCustomer();
                }
                else
                {
                    // caso contrario, editar
                    EditarCustomer(customerId);
                }
            }
        }

        protected void InsertarCustomer()
        {
            // crear un nuevo objeto
            var nuevoCustomer = new Model.Customer();
            // setear los atributos
            nuevoCustomer.FirstName = txtNombre.Text.Trim();
            nuevoCustomer.LastName = txtApellidos.Text.Trim();
            nuevoCustomer.Email = txtEmail.Text.Trim();
            nuevoCustomer.ImageProfile = "https://avatars.dicebear.com/api/male/cibertec.svg";

            // guardar en BD
            using (var uow = new ChinookExistingUnitOfWork())
            {
                uow.Customers.Insert(nuevoCustomer);
                var resultado = uow.Commit();

                // cerrar el modal
                if (resultado > 0)
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "closeModalKey", "closeModal()", true);
                }
            }
        }

        protected void EditarCustomer(int customerId)
        {
            using (var uow = new ChinookExistingUnitOfWork())
            {
                // obtener el customer de la BD
                var oldCustomer = uow.Customers.GetById(customerId);

                // asignar los nuevos campos
                oldCustomer.FirstName = txtNombre.Text.Trim();
                oldCustomer.LastName = txtApellidos.Text.Trim();
                oldCustomer.Email = txtEmail.Text.Trim();
                oldCustomer.ImageProfile = "https://avatars.dicebear.com/api/male/cibertec.svg";

                // hacer un commit
                var resultado = uow.Commit();

                // cerrar el modal
                if (resultado > 0)
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "closeModalKey", "closeModal()", true);
                }
            }
        }

        protected void gvCustomer_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "edit-customer":
                    {
                        // obtener el customer id que queremos editar
                        if(int.TryParse(e.CommandArgument.ToString(), out int customerId) && customerId > 0)
                        {
                            // obtener el customer de la BD
                            var customer = new Model.Customer();
                            using(var uow = new ChinookExistingUnitOfWork())
                            {
                                customer = uow.Customers.GetById(customerId);
                            }
                            // pintar los valores
                            txtNombre.Text = customer.FirstName;
                            txtApellidos.Text = customer.LastName;
                            txtEmail.Text = customer.Email;
                            hdnCustomerId.Value = customer.CustomerId.ToString();

                            // abrir el modal
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "openModalKey", "openModal()", true);
                            litTituloModal.Text = $"Editando el registro con ID {customerId}";
                        }
                        break;
                    }
                case "delete-customer":
                    {
                        // obtener el customer id que queremos eliminar
                        if (int.TryParse(e.CommandArgument.ToString(), out int customerId) && customerId > 0)
                        {
                            using (var uow = new ChinookExistingUnitOfWork())
                            {
                                var deleteCustomer = uow.Customers.GetById(customerId);
                                uow.Customers.Delete(deleteCustomer);
                                uow.Commit();
                            }

                            CargarData();
                        }
                        break;
                    }
                default: break;
            }
        }

        protected void btnError_Click(object sender, EventArgs e)
        {
            throw new Exception("Este es un error de prueba");
        }
    }
}