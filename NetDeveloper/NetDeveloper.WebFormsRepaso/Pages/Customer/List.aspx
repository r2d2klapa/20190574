﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layouts/Main.Master" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="NetDeveloper.WebFormsRepaso.Pages.Customer.List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="alert alert-info" role="alert">
        Bienvenido a la lista de customer
    </div>
    <h1>Lista de Customer</h1>

    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Button runat="server" Text="Insertar" ID="btnInsertar" OnClick="btnInsertar_Click" />
            <asp:Button runat="server" Text="Error" ID="btnError" OnClick="btnError_Click" />
            <asp:GridView runat="server" ID="gvCustomer" AutoGenerateColumns="false" CssClass="table" GridLines="None" AllowPaging="true" OnPageIndexChanging="gvCustomer_PageIndexChanging" OnRowCommand="gvCustomer_RowCommand">
                <Columns>
                    <asp:BoundField DataField="CustomerId" HeaderText="ID" />
                    <asp:BoundField DataField="FirstName" HeaderText="Nombre" />
                    <asp:BoundField DataField="LastName" HeaderText="Apellido" />
                    <asp:ImageField DataImageUrlField="ImageProfile"></asp:ImageField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton runat="server" CommandName="edit-customer" CommandArgument='<%# Eval("CustomerId") %>' CssClass="btn btn-link">Editar</asp:LinkButton>
                            <asp:LinkButton runat="server" CommandName="delete-customer" CommandArgument='<%# Eval("CustomerId") %>' CssClass="btn btn-link btn-warning">Eliminar</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <!-- Modal -->
            <div class="modal fade" id="modalCustomer" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="staticBackdropLabel">
                                <asp:Literal runat="server" ID="litTituloModal"></asp:Literal></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <label>Nombre:</label>
                            <asp:TextBox runat="server" ID="txtNombre"></asp:TextBox>
                            <label>Apellidos:</label>
                            <asp:TextBox runat="server" ID="txtApellidos"></asp:TextBox>
                            <label>Email:</label>
                            <asp:TextBox runat="server" ID="txtEmail"></asp:TextBox>
                        </div>
                        <div class="modal-footer">
                            <asp:HiddenField runat="server" ID="hdnCustomerId" />
                            <asp:Button runat="server" ID="btnSave" Text="Guardar" CssClass="btn btn-primary" OnClick="btnSave_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="cphScripts">
    <script type="text/javascript">
        function openModal() {
            $("#modalCustomer").modal();
        }

        function closeModal() {
            $("#modalCustomer").modal("hide");
        }
    </script>
</asp:Content>
