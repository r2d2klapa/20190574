﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using NetDeveloper.Model.DataContracts;
using NetDeveloper.DataAccess;

namespace NetDeveloper.WcfServices
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "SalesService" en el código y en el archivo de configuración a la vez.
    public class SalesService : ISalesService
    {
        public int Numero()
        {
            return 100;
        }

        public IEnumerable<TrackSaleReportItem> TrackSalesReport()
        {
            using (var unit = new ChinookExistingUnitOfWork())
            {
                var resultado = unit.Tracks.GetTrackSalesReportContract();
                var lista = resultado.ToList();
                return lista;
            }
        }
    }
}
