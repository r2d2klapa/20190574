﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using NetDeveloper.Model.DataContracts;

namespace NetDeveloper.WcfServices
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "ISalesService" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface ISalesService
    {
        /// <summary>
        /// Este contrato va a devolver una colecicón de elementos con la siguiente estructura: Nombre de Canción y Cantidad de veces que se vendió esa canción
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        IEnumerable<TrackSaleReportItem> TrackSalesReport();
        [OperationContract]
        int Numero();
    }
}
