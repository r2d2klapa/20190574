﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetDeveloper.WebForms.Models
{
    /// <summary>
    /// Esta clase será utilizada para armar el menú de la barra lateral
    /// </summary>
    public class SidebarItem
    {
        public string Url { get; set; }
        public string Icon { get; set; }
        public string Name { get; set; }
        public string ActiveClass { get; set; }
    }
}