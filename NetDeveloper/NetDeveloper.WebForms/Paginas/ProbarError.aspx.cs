﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetDeveloper.WebForms.Paginas
{
    public partial class ProbarError : System.Web.UI.Page
    {
        // crear la interface para empezar a loggear mensajes
        private ILog _logger;
        public ProbarError()
        {            
            // instanciar la interface como un objeto de log para esta clase
            _logger = LogManager.GetLogger(typeof(ProbarError));
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGenerarError_Click(object sender, EventArgs e)
        {
            throw new Exception("Este es un error de prueba");
        }

        protected void btnLogInfo_Click(object sender, EventArgs e)
        {
            
            _logger.Info("Nuevo mensaje informativo");
        }

        protected void btnTryCatchError_Click(object sender, EventArgs e)
        {
            try
            {
                // el código botó una excepeción
                throw new Exception("No se pudo conectar a la BD");
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
            }
        }
    }
}