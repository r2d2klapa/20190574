﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout/Site1.Master" AutoEventWireup="true" CodeBehind="ProbarError.aspx.cs" Inherits="NetDeveloper.WebForms.Paginas.ProbarError" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTitulo" runat="server">
    Página para probar un error
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Button runat="server" ID="btnGenerarError" Text="Generar error" OnClick="btnGenerarError_Click" />
    <asp:Button runat="server" ID="btnLogInfo" Text="Log info" OnClick="btnLogInfo_Click" />
    <asp:Button runat="server" ID="btnTryCatchError" Text="Try Catch Error" OnClick="btnTryCatchError_Click" />
</asp:Content>
