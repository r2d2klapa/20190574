﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout/Site1.Master" AutoEventWireup="true" CodeBehind="AlbumTracks.aspx.cs" Inherits="NetDeveloper.WebForms.Paginas.AlbumTracks" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTitulo" runat="server">
    <asp:Label runat="server" ID="lblTitulo"></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="mb-5">
        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button runat="server" ID="btnNuevo" Text="Crear Track" OnClick="btnNuevo_Click" CssClass="btn btn-primary" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:Panel Visible="false" runat="server" ID="pnlVacio" CssClass="d-flex align-items-center flex-column justify-content-center">
        <h2>No se han registrado canciones para este álbum</h2>
        <i class="far fa-folder-open fa-5x"></i>
    </asp:Panel>
    <asp:UpdatePanel runat="server" ID="pnlTracks" Visible="false" UpdateMode="Conditional">
        <ContentTemplate>
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Género</th>
                        <th>Media Type</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="rptTracks" OnItemCommand="rptTracks_ItemCommand" OnItemCreated="rptTracks_ItemCreated">
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <%# Eval("Name") %>
                                </td>
                                <td>
                                    <%# Eval("Genre.Name") %>
                                </td>
                                <td>
                                    <%# Eval("MediaType.Name") %>
                                </td>
                                <td>
                                    <asp:LinkButton runat="server" CssClass="btn btn-link" CommandName="edit" ID="lnkEdit" CommandArgument='<%# Eval("TrackId") %>'>
                            <i class="far fa-edit"></i>
                                    </asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="modalTrack" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <asp:UpdatePanel runat="server" ID="upModal">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Track</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <asp:HiddenField runat="server" ID="hdnIdEditar" Value="0" />
                            <div class="row">
                                <div class="form-group col-12">
                                    <label for="txtNombre">Nombre</label>
                                    <asp:TextBox ClientIDMode="Static" runat="server" ID="txtNombre" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group col-6">
                                    <label for="ddlGenero">Género</label>
                                    <asp:DropDownList runat="server" ID="ddlGenero" ClientIDMode="Static" CssClass="form-control" DataValueField="GenreId" DataTextField="Name"></asp:DropDownList>
                                </div>
                                <div class="form-group col-6">
                                    <label for="ddlMediaType">Media Type</label>
                                    <asp:DropDownList runat="server" ID="ddlMediaType" ClientIDMode="Static" CssClass="form-control" DataValueField="MediaTypeId" DataTextField="Name"></asp:DropDownList>
                                </div>
                                <div class="form-group col-12 col-sm-4">
                                    <label for="txtMs">Ms.</label>
                                    <asp:TextBox ClientIDMode="Static" runat="server" ID="txtMs" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group col-12 col-sm-4">
                                    <label for="txtPrecio">Precio</label>
                                    <asp:TextBox ClientIDMode="Static" runat="server" ID="txtPrecio" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group col-12 col-sm-4">
                                    <label for="txtBytes">Bytes</label>
                                    <asp:TextBox ClientIDMode="Static" runat="server" ID="txtBytes" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <asp:Button runat="server" ID="btnGuardar" CssClass="btn btn-primary" Text="Guardar" OnClick="btnGuardar_Click"></asp:Button>
                        </div>
                    </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <script type="text/javascript">
        function openModal() {
            $("#modalTrack").modal();
        }
    </script>
</asp:Content>
