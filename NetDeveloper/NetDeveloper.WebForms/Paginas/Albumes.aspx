﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Albumes.aspx.cs" Inherits="NetDeveloper.WebForms.Paginas.Albumes" MasterPageFile="~/Layout/Site1.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphTitulo">
    Lista Álbumes
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div>
        <asp:Repeater runat="server" ID="rptAlbumes" OnItemCommand="rptAlbumes_ItemCommand">
            <HeaderTemplate>
                <table class="table table-striped table-sm">
                    <tr>
                        <th>Título
                        </th>
                        <th>Acciones
                        </th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:Label runat="server" ID="lblNombre">
                            <%# Eval("Title") %>
                        </asp:Label>
                        <asp:TextBox Visible="false" runat="server" ID="txtNombre" Text='<%# Eval("Title") %>'>                            
                        </asp:TextBox>
                    </td>
                    <td>
                        <asp:LinkButton Visible="false" runat="server" CssClass="btn btn-link" CommandName="update" CommandArgument='<%# Eval("AlbumId") %>' ID="lnkConfirmEdit">
                            <i class="fas fa-check"></i>
                        </asp:LinkButton>
                        <asp:LinkButton Visible="false" runat="server" CssClass="btn btn-link" ID="lnkCancelEdit" CommandName="cancel-edit">
                            <i class="fas fa-times"></i>
                        </asp:LinkButton>
                        <asp:LinkButton runat="server" CssClass="btn btn-link" CommandName="init-edit" ID="lnkEdit">
                            <i class="far fa-edit"></i>
                        </asp:LinkButton>
                        <asp:LinkButton runat="server" CssClass="btn btn-link" CommandName="delete" CommandArgument='<%# Eval("AlbumId") %>' OnClientClick="return mostrarConfirmacion()" ID="lnkDelete">
                            <i class="far fa-trash-alt"></i>
                        </asp:LinkButton>
                        <asp:LinkButton runat="server" CssClass="btn btn-link" CommandName="tracks" CommandArgument='<%# Eval("AlbumId") %>' ID="lnkTracks">
                            <i class="fas fa-music"></i>
                        </asp:LinkButton>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <asp:Panel runat="server" ID="pnlInsertar">
        <div class="input-group mb-3">
            <asp:TextBox runat="server" ID="txtTitulo" CssClass="form-control" placeholder="Título del álbum"></asp:TextBox>
            <div class="input-group-append">
                <asp:Button runat="server" CssClass="btn btn-outline-secondary" ID="btnGuardar" Text="Insertar" OnClick="btnGuardar_Click" />
            </div>
        </div>
    </asp:Panel>



    <script type="text/javascript">
        function mostrarConfirmacion() {
            console.log("Se va a eliminar un registro");
            return confirm("¿Estás seguro?");
        }
        function mostrarAlerta(mensaje) {
            toastr.info(mensaje)
        }
    </script>

    <asp:Literal runat="server" ID="litScripts">  
    </asp:Literal>

</asp:Content>
