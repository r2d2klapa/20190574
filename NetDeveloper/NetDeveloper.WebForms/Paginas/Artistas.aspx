﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Artistas.aspx.cs" Inherits="NetDeveloper.WebForms.Paginas.Artistas" MasterPageFile="~/Layout/Site1.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="cphTitulo">
    Lista Artista
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Button runat="server" ID="btnInsertarMasivo" CssClass="btn btn-primary btn-sm mb-3" OnClick="btnInsertarMasivo_Click" Text="Insertar Masivo" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Button runat="server" ID="btnNuevo" CssClass="btn btn-primary btn-sm" OnClick="btnNuevo_Click" Text="Crear Artista" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel runat="server" ID="upLista" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="mt-1">
                <asp:Label runat="server" ID="lblInformacion"></asp:Label>
            </div>
            <div>
                <asp:Repeater runat="server" ID="rptArtistas" OnItemCommand="rptArtistas_ItemCommand">
                    <HeaderTemplate>
                        <table class="table table-striped table-sm" id="tabla-artistas">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Álbumes</th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td><%# Eval("Name") %>
                                <asp:LinkButton runat="server" CssClass="btn btn-link ml-2 btn-sm" CommandName="edit" CommandArgument='<%# Eval("ArtistId") %>'>Editar</asp:LinkButton></td>
                            <td><a href="Albumes.aspx?Id=<%# Eval("ArtistId") %>" target="_blank">Ver</a></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <%--Modal para Crear un Artista--%>

    <div class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <asp:UpdatePanel runat="server" ID="upModal">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title">
                                <asp:Label runat="server" ID="lblTituloModal"></asp:Label></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <%--Crear un campo hidden que guardará el valor del Id del artista que está siendo editado--%>
                            <asp:HiddenField runat="server" Value="0" ID="hdnIdEditar" />
                            <div>
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <asp:TextBox runat="server" ID="txtNombre" CssClass="form-control form-control-sm"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <asp:Button runat="server" ID="btnGuardar" OnClick="btnGuardar_Click" Text="Guardar" CssClass="btn btn-primary" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $('#tabla-artistas').DataTable({
                language: {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla =(",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "buttons": {
                        "copy": "Copiar",
                        "colvis": "Visibilidad"
                    }
                }
            });
        })
    </script>

</asp:Content>
