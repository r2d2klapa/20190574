﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetDeveloper.Model.ViewModels;
using NetDeveloper.DataAccess.EF;
using System.Data.Entity;
using Newtonsoft.Json;
using NetDeveloper.DataAccess.Repositories;
using NetDeveloper.DataAccess;

namespace NetDeveloper.WebForms.Paginas
{
    public partial class Dashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var topN = 10;

            var lista = new List<ArtistTopAlbums>();

            // obtener la data de la BD
            using (var unitOfWork = new ChinookExistingUnitOfWork())
            {
                lista = unitOfWork.Artists.GetTopNArtists(topN);
            }

            // 1. Crear el arreglo para mostrar las categorías en el eje x
            var listaCategorias = new List<string>();
            // 2. Crear el arreglo para mostrar la cantidad de albumes que se asignará al campo data de la serie del gráfico
            var listaDataSerie1 = new List<int>();

            // 3. Recorrer la lista de los top artistas y agregar la data
            foreach (var item in lista)
            {
                listaCategorias.Add(item.ArtistName);
                listaDataSerie1.Add(item.AlbumCount);
            }

            // 4. Serializar las listas a JSON
            var listaCategoriasJSON = JsonConvert.SerializeObject(listaCategorias);
            var listaDataSerie1JSON = JsonConvert.SerializeObject(listaDataSerie1);

            // crear arreglo
            // incrustar el valor del literal para setear las variables de JS
            litVariables.Text = $@"<script>
                                var chartTitle = 'Top {topN} artistas';
                                var artistCategoriesArray = {listaCategoriasJSON};
                                var albumCountArray = {listaDataSerie1JSON};
                                </script>";
        }
    }
}