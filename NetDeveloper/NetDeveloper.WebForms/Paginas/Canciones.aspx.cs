﻿using NetDeveloper.DataAccess;
using NetDeveloper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetDeveloper.WebForms.Paginas
{
    public partial class Canciones : PaginaBaseAuth
    {
        public List<Track> ListaCanciones
        {
            get
            {
                // retornar el valor guardado en el ViewState
                if (ViewState["ListaCanciones"] == null)
                {
                    return new List<Track>();
                }

                return ViewState["ListaCanciones"] as List<Track>;
            }
            set
            {
                ViewState["ListaCanciones"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // verificar si el usuario está autenticado
                VerificarUsuario();
                CargarCanciones();

                // hacer el bind de las grillas
                BindTrackAll();
                BindTrack();
            }
        }

        private void CargarCanciones()
        {
            using (var uow = new ChinookExistingUnitOfWork())
            {
                ListaCanciones = uow.Tracks.GetAll();
            }
        }

        protected void BindTrackAll()
        {
            // asignar el valor del data source
            gvTracksAll.DataSource = ListaCanciones;

            // hacer el binding para pintar la data
            gvTracksAll.DataBind();
        }

        protected void BindTrack()
        {
            // asignar el valor del data source
            gvTracks.DataSource = ListaCanciones;

            // hacer el binding para pintar la data
            gvTracks.DataBind();
        }

        protected void gvTracksAll_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvTracksAll.PageIndex = e.NewPageIndex;

            // volver a bindear la data al control
            BindTrackAll();
        }
    }
}