﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetDeveloper.WebForms.Paginas
{
    public partial class EjerciciosAJAX : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnIncrementar_Click(object sender, EventArgs e)
        {
            // obtener el valor actual del contador
            var contador = Convert.ToInt32(lblContador.Text);

            // incrementar el valor del contador
            lblContador.Text = (contador + 1).ToString();

            // obtener el valor actual del contador
            var contador2 = Convert.ToInt32(lblContador2.Text);

            // incrementar el valor del contador
            lblContador2.Text = (contador2 + 1).ToString();

            // actualizar el update panel explicitamente
            upContador2.Update();
        }

        protected void btnAbrirModal_Click(object sender, EventArgs e)
        {

            // registrar un script para abrir el modal
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "openModal", "$('#myModal').modal()", true);

            // modificar el contenido del label
            lblMensajeModal.Text = "Mensaje modificado desde el servidor";

            // actualizar el update panel
            upModal.Update();
        }
    }
}