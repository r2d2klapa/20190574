﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetDeveloper.DataAccess.EF;
using NetDeveloper.Model;

namespace NetDeveloper.WebForms.Paginas
{
    public partial class Albumes : PaginaBaseAuth
    {
        public int ArtistId
        {
            get
            {
                // retornar el valor del parámetro del query string
                var artistId = 0;

                if (int.TryParse(Request.QueryString["Id"], out artistId))
                {
                    return artistId;
                }

                return 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // antes de todo, validar auth
                VerificarUsuario();
                // validar autorización
                VerificarAutorizacion();
                CargaInicial();
            }
        }

        private void VerificarAutorizacion()
        {
            // verificar la visibilidad de la página
            var autorizadoAVer = VerificarRolDelUsuario(new[] { "admin" });

            if (!autorizadoAVer)
            {
                // si no esta autorizado a ver, redireccionar al login
                RedireccionarAlLogin();
            }

            var estaAutorizadoAInsertar = VerificarRolDelUsuario(new[] { "admin", "mantainer" });

            pnlInsertar.Visible = estaAutorizadoAInsertar;
        }

        protected void CargaInicial()
        {
            CargarListaDeAlbumes(ArtistId);
        }

        private void CargarListaDeAlbumes(int artistId)
        {
            // significa que es un id válido
            // obtener la lista de album de la BD
            var listaAlbumes = new List<Album>();

            using (var context = new ChinookContext())
            {
                listaAlbumes = context.Albums.Where(a => a.ArtistId == artistId).ToList();
            }

            // asignamos la fuente de datos al repeater
            rptAlbumes.DataSource = listaAlbumes;
            rptAlbumes.DataBind();
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            // insertar el album
            // obtener el titulo del nuevo album
            var titulo = txtTitulo.Text;

            if (string.IsNullOrEmpty(titulo))
            {
                return;
            }

            // crear el objeto nuevo a insertar
            var nuevoAlbum = new Album() { Title = titulo, ArtistId = ArtistId };

            // insertamos a la BD
            using (var context = new ChinookContext())
            {
                context.Albums.Add(nuevoAlbum);
                context.SaveChanges();
            }

            if (nuevoAlbum.AlbumId > 0)
            {
                // significa que se ha registrado correctamente
                txtTitulo.Text = string.Empty;
                CargarListaDeAlbumes(ArtistId);
            }
        }

        protected void rptAlbumes_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "delete":
                    {
                        // cuando se quiere eliminar el registro
                        // obtener el id a eliminar
                        var id = Convert.ToInt32(e.CommandArgument);

                        using (var context = new ChinookContext())
                        {
                            // obtener el album a eliminar
                            var album = context.Albums.Find(id);

                            if (album == null)
                            {
                                // significa que el id del album no existe
                                return;
                            }

                            context.Albums.Remove(album);
                            context.SaveChanges();
                        }

                        CargarListaDeAlbumes(ArtistId);

                        // mostrar una alerta en JS

                        litScripts.Text = @"
                            <script>
                                mostrarAlerta('Se eliminó satisfactoriamente');
                            </script>";

                        break;
                    }
                case "init-edit":
                    {
                        // obtener la fila 
                        var item = (e.CommandSource as LinkButton).Parent as RepeaterItem;
                        MostrarOcultarElementos(item, true);

                        break;
                    }
                case "cancel-edit":
                    {
                        // obtener la fila 
                        var item = (e.CommandSource as LinkButton).Parent as RepeaterItem;
                        MostrarOcultarElementos(item, false);

                        break;
                    }
                case "update":
                    {
                        var id = Convert.ToInt32(e.CommandArgument);
                        // obtener la fila 
                        var item = (e.CommandSource as LinkButton).Parent as RepeaterItem;
                        var txtNombre = item.FindControl("txtNombre") as TextBox;
                        var nuevoTitulo = txtNombre.Text;

                        // actualizar en BD
                        using (var context = new ChinookContext())
                        {
                            var album = context.Albums.Find(id);
                            album.Title = nuevoTitulo;
                            context.SaveChanges();
                        }
                        CargarListaDeAlbumes(ArtistId);
                        break;
                    }
                case "tracks":
                    {
                        Response.Redirect($"~/Paginas/AlbumTracks.aspx?id={e.CommandArgument}");
                        break;
                    }
                default: break;
            }
        }

        private static void MostrarOcultarElementos(RepeaterItem item, bool modoEdicion)
        {
            var lblNombre = item.FindControl("lblNombre") as Label;
            var txtNombre = item.FindControl("txtNombre") as TextBox;

            var linkButtonCheck = item.FindControl("lnkConfirmEdit") as LinkButton;
            var linkButtonCancel = item.FindControl("lnkCancelEdit") as LinkButton;
            var linkButtonEdit = item.FindControl("lnkEdit") as LinkButton;
            var linkButtonDelete = item.FindControl("lnkDelete") as LinkButton;
            var linkButtonTracks = item.FindControl("lnkTracks") as LinkButton;

            // mostramos y ocultamos los controles
            txtNombre.Visible = modoEdicion;
            linkButtonCancel.Visible = modoEdicion;
            linkButtonCheck.Visible = modoEdicion;

            lblNombre.Visible = !modoEdicion;
            linkButtonEdit.Visible = !modoEdicion;
            linkButtonDelete.Visible = !modoEdicion;
            linkButtonTracks.Visible = !modoEdicion;
        }
    }
}