﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetDeveloper.DataAccess;
using NetDeveloper.DataAccess.EF;
using NetDeveloper.Model;

namespace NetDeveloper.WebForms.Paginas
{
    public partial class Artistas : PaginaBaseAuth
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // verificar si el usuario está autenticado
                VerificarUsuario();
                CargarLista();
            }
        }

        private void CargarLista()
        {
            // obtener la lista de artistas de la BD
            var listaArtistas = new List<Artist>();

            using (var unitOfWork = new ChinookExistingUnitOfWork())
            {
                listaArtistas = unitOfWork.Artists.GetAll();
            }

            // mostrar la cantidad de artistas en el label
            lblInformacion.Text = $"Existen {listaArtistas.Count} artistas en la base de datos";

            //var listaPrueba = new string[] { "1", "2", "3" };

            // pasar la lista como fuente de datos del repeater
            rptArtistas.DataSource = listaArtistas;
            //rptArtistas.DataSource = listaPrueba;
            rptArtistas.DataBind();
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            hdnIdEditar.Value = "0";
            OpenModal();
        }

        private void OpenModal()
        {
            lblTituloModal.Text = hdnIdEditar.Value == "0" ? "Nuevo Artista" : $"Editar un artista - {hdnIdEditar.Value}";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "openmodal", "$('.modal').modal()", true);
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            // obtener el texto para el nuevo artista
            var nombre = txtNombre.Text;

            if (string.IsNullOrEmpty(nombre))
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "showAlert", "toastr.error('Ingresa un nombre válido');", true);
                return;
            }

            if (hdnIdEditar.Value == "0")
            {
                InsertarArtista(nombre);
            }
            else
            {
                EditarArtista(hdnIdEditar.Value, nombre);
            }


            // limpiar el formulario
            txtNombre.Text = "";

            // cargar la lista nuevamente
            CargarLista();

            // actualizar el update panel de la lista
            upLista.Update();

            // mostrar el mensaje satisfactorio
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "showAlertSuccess", "toastr.success('Se guardaron los cambios correctamente');", true);
            // cerrar el modal
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "hidemodal", "$('.modal').modal('hide')", true);
        }

        private void EditarArtista(string value, string nombre)
        {
            // obtener el artista a editar
            Artist artistaEditar;

            using (var context = new ChinookContext())
            {

                artistaEditar = context.Artists.Find(Convert.ToInt32(value));

                // asignar los nuevos valores
                artistaEditar.Name = nombre;

                // guardar los cambios
                context.SaveChanges();
            }
        }

        private static void InsertarArtista(string nombre)
        {
            // insertar el nuevo artista
            using (var context = new ChinookContext())
            {
                context.Artists.Add(new Artist { Name = nombre });
                context.SaveChanges();
            }
        }

        protected void rptArtistas_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "edit":
                    {
                        var commandArgument = e.CommandArgument.ToString();
                        var idEditar = Convert.ToInt32(commandArgument);

                        // obtener la información del artista a editar
                        Artist artistaEditar;
                        using (var context = new ChinookContext())
                        {
                            artistaEditar = context.Artists.Find(idEditar);
                        }

                        // setear los valores del formulario de artista
                        txtNombre.Text = artistaEditar.Name;

                        hdnIdEditar.Value = idEditar.ToString();
                        // abrir el modal
                        OpenModal();
                        break;
                    }
                default: break;
            }
        }

        protected void btnInsertarMasivo_Click(object sender, EventArgs e)
        {
            using (var context = new ChinookContext())
            {
                // insertar 10,000 registros a artista
                for (int i = 0; i < 9999; i++)
                {
                    context.Artists.Add(new Artist { Name = $"Artista - {i + 1}" });
                }

                context.SaveChanges();
            }

            // mostrar el mensaje satisfactorio
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "showAlertSuccess2", "toastr.success('Se guardaron los cambios correctamente - masivo');", true);
        }
    }
}