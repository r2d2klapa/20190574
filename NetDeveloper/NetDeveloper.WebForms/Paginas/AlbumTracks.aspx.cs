﻿using NetDeveloper.Model;
using NetDeveloper.DataAccess.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;

namespace NetDeveloper.WebForms.Paginas
{
    public partial class AlbumTracks : System.Web.UI.Page
    {
        // esta propiedad la vamos a utilizar para obtener el id que proviene del query string de la solicitud
        public int IdAlbum
        {
            get
            {
                if (int.TryParse(Request.QueryString["id"], out int id))
                {
                    return id;
                }

                return 0;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CargarComponentes();
            }
        }

        public void CargarComponentes()
        {
            List<Track> listaTracks;
            Album album;

            using (var context = new ChinookContext())
            {
                // buscar en BD si existe un registro con el id indicado en el query string de la solicitud
                // la función Include sirve para tambien traer la información de las tablas relacionadas
                //album = context.Albums
                //    .Include(a => a.Tracks.Select(t => t.Genre))
                //    .Include(a => a.Tracks.Select(t => t.MediaType))
                //    .FirstOrDefault(a => a.AlbumId == IdAlbum);

                listaTracks = context.Tracks
                    .Include(t => t.Album)
                    .Include(t => t.Genre)
                    .Include(t => t.MediaType)
                    .Where(t => t.AlbumId == IdAlbum).ToList();

                album = context.Albums.Find(IdAlbum);
            }

            if (album == null)
            {
                // mostrar un mensaje o redireccionar a una página 404
                return;
            }

            // setear los valores a los controles
            lblTitulo.Text = album.Title;
            pnlVacio.Visible = listaTracks.Count == 0;
            if (listaTracks.Count > 0)
            {
                pnlTracks.Visible = true;
                rptTracks.DataSource = listaTracks;
                rptTracks.DataBind();
            }

            // cargarComponentes los desplegables del formulario track
            using (var context = new ChinookContext())
            {
                ddlGenero.DataSource = context.Genres.ToList();
                ddlGenero.DataBind();
                ddlMediaType.DataSource = context.MediaTypes.ToList();
                ddlMediaType.DataBind();
            }
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            hdnIdEditar.Value = "0";
            AbrirModalTrack();
        }

        private void AbrirModalTrack()
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "openModal", "openModal()", true);
        }

        private void CerrarModalTrack()
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "closeModal", "$('#modalTrack').modal('hide')", true);
        }

        protected void rptTracks_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "edit":
                    {
                        // abrir el modal
                        AbrirModalTrack();
                        // cargar la data del modal
                        var trackId = Convert.ToInt32(e.CommandArgument);
                        CargarDataModal(trackId);
                        break;
                    }
                default: break;
            }
        }

        private void CargarDataModal(int trackId)
        {
            // obtener la información del track de BD
            Track track = null;

            using (var context = new ChinookContext())
            {
                track = context.Tracks.Find(trackId);
            }

            txtNombre.Text = track.Name;
            txtMs.Text = track.Milliseconds.ToString();
            txtPrecio.Text = track.UnitPrice.ToString();
            txtBytes.Text = track.Bytes.ToString();
            ddlGenero.SelectedValue = track.GenreId.ToString();
            ddlMediaType.SelectedValue = track.MediaTypeId.ToString();
            hdnIdEditar.Value = track.TrackId.ToString();
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            // obtener los valores que se vana guardar
            var cancionGuardar = new Track
            {
                Name = txtNombre.Text,
                GenreId = Convert.ToInt32(ddlGenero.SelectedValue),
                MediaTypeId = Convert.ToInt32(ddlMediaType.SelectedValue),
                Milliseconds = Convert.ToInt32(txtMs.Text),
                UnitPrice = Convert.ToDecimal(txtPrecio.Text),
                Bytes = Convert.ToInt32(txtBytes.Text),
                AlbumId = IdAlbum
            };

            if (hdnIdEditar.Value == "0")
            {
                InsertarCancion(cancionGuardar);
            }
            else
            {
                // primero asignar el id del elemento a editar
                cancionGuardar.TrackId = Convert.ToInt32(hdnIdEditar.Value);
                EditarCancion(cancionGuardar);
            }

            // cerrar el modal
            CerrarModalTrack();

            // @todo: limpiar los campos

            // actualizar la lista
            CargarComponentes();

            // actualizar el update panel
            pnlTracks.Update();

            // mostrar la alerta
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "showAlertSuccess", "toastr.success('Se guardaron los cambios correctamente');", true);
        }

        private void EditarCancion(Track cancionGuardar)
        {
            // editar la cancion (otra forma)
            using (var context = new ChinookContext())
            {
                context.Entry<Track>(cancionGuardar).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        private void InsertarCancion(Track cancionGuardar)
        {
            using (var context = new ChinookContext())
            {
                context.Tracks.Add(cancionGuardar);
                context.SaveChanges();
            }
        }

        protected void rptTracks_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            // para decirle al script manager que los botones de editar de cada fila también deben actualizar el update panel por ajax
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var sm = ScriptManager.GetCurrent(Page);
                var lnkBtn = e.Item.FindControl("lnkEdit");
                sm.RegisterAsyncPostBackControl(lnkBtn);
            }
        }
    }
}