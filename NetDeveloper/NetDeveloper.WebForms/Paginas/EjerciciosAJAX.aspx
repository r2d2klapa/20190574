﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EjerciciosAJAX.aspx.cs" Inherits="NetDeveloper.WebForms.Paginas.EjerciciosAJAX" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Ejercicios AJAX</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />
</head>
<body>
    <form id="form1" runat="server">
        <%-- Este control es necesario para poder habilitar AJAX en nuestra página ASPX --%>
        <asp:ScriptManager EnablePartialRendering="true" runat="server" ID="smMain">
            <Scripts>
                <%-- Agregar las referencias a JS aquí --%>
                <asp:ScriptReference Path="https://code.jquery.com/jquery-3.4.1.min.js" />
                <asp:ScriptReference Path="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" />
            </Scripts>
        </asp:ScriptManager>
        <asp:UpdatePanel runat="server" ID="upContador">
            <ContentTemplate>
                <div>
                    <asp:Button runat="server" ID="btnIncrementar" OnClick="btnIncrementar_Click" Text="Incrementar" />
                </div>
                <div>
                    <asp:Label runat="server" ID="lblContador" Text="0"></asp:Label>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel runat="server" ID="upContador2" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Label runat="server" ID="lblContador2" Text="0"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>

        <div>
            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Button runat="server" ID="btnAbrirModal" Text="Abrir Modal" OnClick="btnAbrirModal_Click" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <button type="button" onclick="abrirModal();">Abrir Modal Cliente</button>

        <div id="myModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <asp:UpdatePanel runat="server" ID="upModal" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-content">
                            <h1>Contenido del modal</h1>
                            <asp:Label runat="server" ID="lblMensajeModal">Mensaje por defecto</asp:Label>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>

        <script>
            function abrirModal() {
                $("#myModal").modal();
            }
        </script>
    </form>
</body>
</html>
