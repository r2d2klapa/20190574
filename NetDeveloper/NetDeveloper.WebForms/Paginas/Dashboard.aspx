﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout/Site1.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="NetDeveloper.WebForms.Paginas.Dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../scripts/highcharts.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTitulo" runat="server">
    Dashboard de la aplicación
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="container" style="width: 100%; height: 400px;"></div>
    <asp:Literal runat="server" ID="litVariables"></asp:Literal>

    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function () {


            var data = {
                chart: {
                    type: 'column'
                },
                title: {
                    text: chartTitle
                },
                xAxis: {
                    categories: artistCategoriesArray
                },
                yAxis: {
                    title: {
                        text: 'Cantidad de Álbumes'
                    }
                },
                series: [{
                    name: "Artista",
                    data: albumCountArray
                }]

            }

            var myChart = Highcharts.chart('container', data);
        });
    </script>
</asp:Content>
