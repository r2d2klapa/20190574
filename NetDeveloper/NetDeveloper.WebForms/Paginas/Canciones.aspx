﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout/Site1.Master" AutoEventWireup="true" CodeBehind="Canciones.aspx.cs" Inherits="NetDeveloper.WebForms.Paginas.Canciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTitulo" runat="server">
    Lista de Canciones
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-all-tab" data-toggle="tab" href="#nav-all" role="tab" aria-controls="nav-home" aria-selected="true">Ver todo</a>
            <a class="nav-item nav-link" id="nav-custom-tab" data-toggle="tab" href="#nav-custom" role="tab" aria-controls="nav-profile" aria-selected="false">Ver personalizado</a>
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-all" role="tabpanel" aria-labelledby="nav-all-tab">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <asp:GridView runat="server" ID="gvTracksAll" CssClass="table table-bordered" AllowPaging="true" OnPageIndexChanging="gvTracksAll_PageIndexChanging" PageSize="50"></asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="tab-pane fade" id="nav-custom" role="tabpanel" aria-labelledby="nav-custom-tab">
            <asp:GridView runat="server" ID="gvTracks" AutoGenerateColumns="false">
                <Columns>
                    <asp:BoundField HeaderText="Nombre" DataField="Name" />
                    <asp:BoundField HeaderText="Precio" DataField="UnitPrice" />
                    <asp:BoundField HeaderText="Bytes" DataField="Bytes" />
                </Columns>
            </asp:GridView>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%= gvTracks.ClientID%>').prepend($("<thead></thead>").append($("#<%= gvTracks.ClientID%>").find("tr:first"))).DataTable({
                stateSave: true,                
            })
            
        });
    </script>
</asp:Content>
