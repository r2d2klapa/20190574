﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetDeveloper.WebForms
{
    public class PaginaBaseAuth : System.Web.UI.Page
    {
        protected void VerificarUsuario()
        {
            // esta función va a validar si el usuario está autenticado
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                // si el usuario no está autenticado, redireccionamos al login
                RedireccionarAlLogin();
            }
        }

        protected void RedireccionarAlLogin()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                // si el usuario está autenticado, cerrar la sesión antes de redireccionar al login
                var authContext = Request.GetOwinContext().Authentication;
                authContext.SignOut("ApplicationCookie");
            }
            Response.Redirect("~/Account/Login.aspx");
        }

        protected bool VerificarRolDelUsuario(string[] roles)
        {
            // si el usuario no tiene el rol que se ha enviado, redireccionar al login y cerrar sesión            
            foreach (var rol in roles)
            {
                if (HttpContext.Current.User.IsInRole(rol))
                {
                    return true;
                }
            }

            // si llegamos a esta parte del código significa que el usuario no tiene ninguno de los roles solicitados
            return false;
        }
    }
}