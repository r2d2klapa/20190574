﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using log4net;

namespace NetDeveloper.WebForms
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            // cofigurar el logger en base a lo indicado en el web config
            log4net.Config.XmlConfigurator.Configure();
        }

        protected void Application_Error(Object sender, EventArgs e)
        {
            // obtener la excepción
            var ex = Server.GetLastError();
            var logger = LogManager.GetLogger(typeof(Global));
            // escribir el mensaje de error
            logger.Error(ex);
        }
    }
}