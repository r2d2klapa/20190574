﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Claims;
using NetDeveloper.DataAccess.EF;
using NetDeveloper.Model;
using Microsoft.Owin.Security;
using NetDeveloper.DataAccess;

namespace NetDeveloper.WebForms.Account
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // verificar si el usuario ya inició sesión
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Response.Redirect("~/Paginas/Dashboard.aspx");
            }
        }

        protected void btnIniciar_Click(object sender, EventArgs e)
        {
            // 1. Obtener el usuario de la BD
            User usuario = null;

            using (var unitOfWork = new ChinookExistingUnitOfWork())
            {
                // obtener los valores que el usuario ha ingresado en la página
                var email = txtEmail.Text;
                var password = txtPassword.Text;
                usuario = unitOfWork.Users.GetUserByEmailAndPassword(email, password);
            }

            if (usuario == null)
            {
                // si el usuario no existe, significa que las credenciale sson inválidas
                //  se podría mostrar un mensaje de error
                lblMensaje.Text = "Usuario o contraseña inválidos";
                lblMensaje.Visible = true;
                return;
            }

            // 2. Crear la identidad del usuario
            var identidad = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Email,usuario.Email),
                new Claim(ClaimTypes.Name, usuario.Name),
                new Claim("DNI", usuario.Dni)
            }, "ApplicationCookie");

            // 2.1. Agregar los roles del usuario
            // "admin,user,mantainer"
            // ["admin","user","mantainer"]
            var arregloRoles = usuario.Roles.Split(',');

            foreach (var rol in arregloRoles)
            {
                identidad.AddClaim(new Claim(ClaimTypes.Role, rol));
            }

            // 3. Iniciar Sesión (crear la cookie de autenticación)
            // 3.1. Obtener el contexto de autenticación
            var authContext = Request.GetOwinContext().Authentication;

            // 3.2. Iniciar sesión con la identidad que se ha creado en el paso 2
            authContext.SignIn(identidad);

            // 4. Redireccionar a la página principal del app
            Response.Redirect("~/Paginas/Artistas.aspx");
        }

        protected void btnIniciarGoogle_Click(object sender, EventArgs e)
        {
            // hacer el challenge hacia Google
            Context.GetOwinContext().Authentication.Challenge(new AuthenticationProperties
            {
                RedirectUri = ResolveUrl("~/Account/ExternalLoginCallback.aspx")
            }, "Google");
        }
    }
}