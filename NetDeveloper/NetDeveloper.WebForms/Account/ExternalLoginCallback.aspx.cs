﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Owin.Security;
using System.Security.Claims;

namespace NetDeveloper.WebForms.Account
{
    public partial class ExternalLoginCallback : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // cuando la página cargue hacer la lógica de iniciar sesión con la información que devuelve Google
            if (!IsPostBack)
            {
                // 1. obtener la información que nos devuelve Google
                var googleLoginInfo = Context.GetOwinContext().Authentication.GetExternalLoginInfo();

                if (googleLoginInfo == null)
                {
                    // redireciconar nuevamente al login
                    Response.Redirect("~/Account/Login.aspx");
                }

                // 2. obtener los claims de google
                var googleClaims = googleLoginInfo.ExternalIdentity.Claims.ToList();

                var emailClaim = googleClaims.FirstOrDefault(c => c.Type == ClaimTypes.Email);
                var nameClaim = googleClaims.FirstOrDefault(c => c.Type == ClaimTypes.Name);
                var nameIdentifierClaim = googleClaims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier);

                // 3. Con los claims de Google, crear la identidad del usuario para el aplicativo, tal cual se hace en login
                var identidad = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Email, emailClaim.Value),
                    new Claim(ClaimTypes.Name, nameClaim.Value),
                    new Claim("DNI", "00000000")
                }, "ApplicationCookie");

                // 4. Agregar un rol por defecto a todos los usuarios que inicien sesión con Google
                identidad.AddClaim(new Claim(ClaimTypes.Role, "user"));

                // 5. Iniciar Sesión en el aplicativo
                var authContext = Request.GetOwinContext().Authentication;
                authContext.SignIn(identidad);

                // 6. Redireccionar a una página
                Response.Redirect("~/Paginas/Artistas.aspx");
            }
        }
    }
}