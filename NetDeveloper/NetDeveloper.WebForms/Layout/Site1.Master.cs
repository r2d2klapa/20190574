﻿using NetDeveloper.WebForms.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetDeveloper.WebForms.Layout
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                // si el usuario no está autenticado, redireccionar al login
                Response.Redirect("~/Account/Login.aspx");
            }
            CargarDatosUsuario();
            CargarMenu();
        }

        private void CargarDatosUsuario()
        {
            // obtener el arreglo con el claims identity
            var identidad = HttpContext.Current.User.Identity as ClaimsIdentity;

            // convertir el arreglo a una lista para aplicar linq
            var listaClaims = identidad.Claims.ToList();

            // obtener el claim que queremos utilizar
            var dni = listaClaims.FirstOrDefault(c => c.Type == "DNI");

            var email = listaClaims.FirstOrDefault(c => c.Type == ClaimTypes.Email);

            var nombreUsuario = HttpContext.Current.User.Identity.Name;
            lblNombreUsuario.Text = $"Bienvenido, {nombreUsuario} - DNI: {dni.Value} - Email: {email.Value}";
        }

        public void CargarMenu()
        {
            // caragar los items del menu lateral (sidebar)
            rptSidebarMenu.DataSource = new List<SidebarItem> {
                new SidebarItem {
                    Url = "a", Icon = "fas fa-home", Name = "Inicio", ActiveClass="active"
                },
                new SidebarItem {
                    Url = Page.ResolveUrl("~/Paginas/Artistas.aspx"), Icon = "fas fa-headphones-alt", Name = "Artistas"
                }
            };
            rptSidebarMenu.DataBind();
        }

        protected void lnkCerrarSesion_Click(object sender, EventArgs e)
        {
            // Obtener el contexto de autenticación
            var authContext = Request.GetOwinContext().Authentication;

            // cerar la sesión y luego redireciconar al login
            authContext.SignOut("ApplicationCookie");

            Response.Redirect("~/Account/Login.aspx");
        }
    }
}