﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout/Site1.Master" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="NetDeveloper.WebForms.ErrorPages.Error" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTitulo" runat="server">
    Oops, ocurrió un error
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        Regresar al <asp:LinkButton runat="server" PostBackUrl="~/Paginas/Artistas.aspx">Inicio</asp:LinkButton>
    </div>
</asp:Content>
