﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Owin.Security.Google;
using Microsoft.AspNet.Identity;

[assembly: OwinStartup(typeof(NetDeveloper.WebForms.Startup))]

namespace NetDeveloper.WebForms
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigurarAutenticacion(app);
        }

        private void ConfigurarAutenticacion(IAppBuilder app)
        {
            // configurar la autenticación por cookies
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = "ApplicationCookie",
                CookieName = "DeveloperAuthCookie",
                LoginPath = new PathString("/Account/Login.aspx")
            });

            // indicar al aplicativo que también se va a utilizar un mecanismo de autenticación externo (Google)
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // configurar el inicio de sesión con Google
            app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions
            {
                // indicar el id cliente y el secreto que hemos obtenido al configurar en la consola de desarrolladores de Google
                ClientId = "819502966171-8vjh2vrg9nhat42tv50u35spvebfempa.apps.googleusercontent.com",
                ClientSecret = "Q693yFySwCeQ79_5xALd35tC"
            });

        }
    }
}
