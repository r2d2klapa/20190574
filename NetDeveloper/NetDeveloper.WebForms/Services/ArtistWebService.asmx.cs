﻿using NetDeveloper.DataAccess;
using NetDeveloper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace NetDeveloper.WebForms.Services
{
    /// <summary>
    /// Descripción breve de ArtistWebService
    /// </summary>    
    [ScriptService]
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class ArtistWebService : System.Web.Services.WebService
    {
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        [WebMethod]
        public List<Artist> ObtenerTodos()
        {
            // esta función va a validar si el usuario está autenticado
            //if (!HttpContext.Current.User.Identity.IsAuthenticated)
            //{
            //    return new List<Artist>();
            //}

            using (var uow = new ChinookExistingUnitOfWork())
            {
                return uow.Artists.GetAll();
            }
        }

        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        [WebMethod]
        public Artist ObtenerPorId(int id)
        {
            using (var uow = new ChinookExistingUnitOfWork())
            {
                return uow.Artists.GetById(id);
            }
        }

        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [WebMethod]
        public int Insertar(Artist nuevoArtista)
        {
            using (var uow = new ChinookExistingUnitOfWork())
            {
                uow.Artists.Insert(nuevoArtista);
                uow.Commit();
                return nuevoArtista.ArtistId;
            }
        }
    }
}
